/********************************************************************************
** Form generated from reading UI file 'plansza.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLANSZA_H
#define UI_PLANSZA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Plansza
{
public:
    QLabel *label;
    QLabel *label_14;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_15;
    QLabel *label_16;
    QLabel *label_17;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *label_20;
    QLabel *label_21;
    QLabel *label_22;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_25;
    QLabel *label_26;

    void setupUi(QWidget *Plansza)
    {
        if (Plansza->objectName().isEmpty())
            Plansza->setObjectName(QString::fromUtf8("Plansza"));
        Plansza->resize(1117, 740);
        label = new QLabel(Plansza);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(0, 0, 801, 741));
        label->setPixmap(QPixmap(QString::fromUtf8("../images/Szachownica.jpg")));
        label_14 = new QLabel(Plansza);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(830, 420, 67, 17));
        label_2 = new QLabel(Plansza);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(680, 560, 91, 91));
        label_2->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_3 = new QLabel(Plansza);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(590, 460, 91, 91));
        label_3->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_4 = new QLabel(Plansza);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(490, 560, 91, 91));
        label_4->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_5 = new QLabel(Plansza);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(590, 650, 91, 91));
        label_5->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_6 = new QLabel(Plansza);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(400, 650, 91, 91));
        label_6->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_7 = new QLabel(Plansza);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(310, 560, 91, 91));
        label_7->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_8 = new QLabel(Plansza);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(400, 460, 91, 91));
        label_8->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_9 = new QLabel(Plansza);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(210, 460, 91, 91));
        label_9->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_10 = new QLabel(Plansza);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(120, 560, 91, 91));
        label_10->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_11 = new QLabel(Plansza);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(210, 650, 91, 91));
        label_11->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_12 = new QLabel(Plansza);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(20, 650, 91, 91));
        label_12->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_13 = new QLabel(Plansza);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(20, 470, 91, 91));
        label_13->setPixmap(QPixmap(QString::fromUtf8("../images/bia\305\202y.png")));
        label_15 = new QLabel(Plansza);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setGeometry(QRect(680, 0, 91, 91));
        label_15->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_16 = new QLabel(Plansza);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setGeometry(QRect(590, 90, 91, 91));
        label_16->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_17 = new QLabel(Plansza);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(680, 180, 91, 91));
        label_17->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_18 = new QLabel(Plansza);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setGeometry(QRect(490, 180, 91, 91));
        label_18->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_19 = new QLabel(Plansza);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setGeometry(QRect(400, 90, 91, 91));
        label_19->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_20 = new QLabel(Plansza);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setGeometry(QRect(500, 0, 91, 91));
        label_20->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_21 = new QLabel(Plansza);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setGeometry(QRect(210, 90, 91, 91));
        label_21->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_22 = new QLabel(Plansza);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setGeometry(QRect(310, 180, 91, 91));
        label_22->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_23 = new QLabel(Plansza);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setGeometry(QRect(120, 180, 91, 91));
        label_23->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_24 = new QLabel(Plansza);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setGeometry(QRect(20, 90, 91, 91));
        label_24->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_25 = new QLabel(Plansza);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setGeometry(QRect(310, 0, 91, 91));
        label_25->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));
        label_26 = new QLabel(Plansza);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setGeometry(QRect(120, 0, 91, 91));
        label_26->setPixmap(QPixmap(QString::fromUtf8("../images/czarny.png")));

        retranslateUi(Plansza);

        QMetaObject::connectSlotsByName(Plansza);
    } // setupUi

    void retranslateUi(QWidget *Plansza)
    {
        Plansza->setWindowTitle(QCoreApplication::translate("Plansza", "Warcaby", nullptr));
        label->setText(QString());
        label_14->setText(QString());
        label_2->setText(QString());
        label_3->setText(QString());
        label_4->setText(QString());
        label_5->setText(QString());
        label_6->setText(QString());
        label_7->setText(QString());
        label_8->setText(QString());
        label_9->setText(QString());
        label_10->setText(QString());
        label_11->setText(QString());
        label_12->setText(QString());
        label_13->setText(QString());
        label_15->setText(QString());
        label_16->setText(QString());
        label_17->setText(QString());
        label_18->setText(QString());
        label_19->setText(QString());
        label_20->setText(QString());
        label_21->setText(QString());
        label_22->setText(QString());
        label_23->setText(QString());
        label_24->setText(QString());
        label_25->setText(QString());
        label_26->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Plansza: public Ui_Plansza {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLANSZA_H
