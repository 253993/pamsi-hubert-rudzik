cmake_minimum_required(VERSION 3.11)

project(Warcaby VERSION 0.1 LANGUAGES CXX)

add_executable(Warcaby
        src/main.cpp
        src/Pawn.cpp
        src/Board.cpp
        src/Scene.cpp
        src/Conditions.cpp)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

target_compile_options(Warcaby PRIVATE -Werror)

target_compile_features(Warcaby PRIVATE cxx_std_17)

# Find SFML
find_package(SFML 2.5 COMPONENTS audio graphics window system REQUIRED)

#Include SOL2 header files
include_directories(${CMAKE_SOURCE_DIR}/sol2/include)

#add_subdirectory(ECS)

target_link_libraries(Warcaby
        PUBLIC
        sfml-graphics sfml-audio sfml-window sfml-system )
