//
// Created by hubert on 23.05.2021.
//

#ifndef WARCABY_BOARD_H
#define WARCABY_BOARD_H
#define SIZE 8

#include<SFML/Graphics.hpp>
#include<iostream>
#include <time.h>
#include <limits.h>

using namespace sf;
class Board{
/* -1=Czarny Zero = puste 1=Biały */
    int board[SIZE][SIZE]={
            0,-1, 0,-1, 0 ,-1, 0,-1,
            -1, 0,-1, 0,-1, 0,-1, 0,
            0,-1, 0,-1, 0,-1, 0,-1,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            1, 0, 1, 0, 1, 0, 1, 0,
            0, 1, 0, 1, 0, 1, 0, 1,
            1, 0, 1, 0, 1, 0, 1, 0,
    };
    int boardcol[SIZE][SIZE]={
            0,1, 0,1, 0 ,1, 0,1,
            1, 0,1, 0,1, 0,1, 0,
            0,1, 0,1, 0,1, 0,1,
            1, 0, 1, 0, 1, 0, 1, 0,
            0, 1, 0, 1, 0, 1, 0, 1,
            1, 0, 1, 0, 1, 0, 1, 0,
            0, 1, 0, 1, 0, 1, 0, 1,
            1, 0, 1, 0, 1, 0, 1, 0,
    };

    Texture t;
public:
    int getBoardcol(int x, int y) const{
        return boardcol[x][y];}
    int getBoard (int x,int y) const{
        return board[x][y];
    }
    void setBoard(int x,int y,int val){
        board[x][y]=val;
    }
   Texture& load();
};


#endif //WARCABY_BOARD_H
