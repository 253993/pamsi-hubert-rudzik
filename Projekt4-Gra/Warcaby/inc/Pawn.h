//
// Created by hubert on 23.05.2021.
//

#ifndef WARCABY_PAWN_H
#define WARCABY_PAWN_H
#include <algorithm>
#include <SFML/Graphics.hpp>
#define sizeb 95

using namespace sf;
class Pawns
{
    unsigned black_num=12;
    unsigned white_num=12;
     Texture t_pawns[24];
public:

    Texture getTexture(int i){
        return t_pawns[i];
    }
    void setTexture(int i,std::string  plik)
    {
        t_pawns[i].loadFromFile(plik);
    }
    void setWhiteNum(){
        white_num--;
    }
    void setBlackNum(){
        black_num--;
    }
    void setWhiteNum_plus(){
        white_num++;
    }
    void setBlackNum_plus(){
        black_num++;
    }
     unsigned getWhiteNum() const{
        return white_num;
    }
    unsigned getBlackNum() const{
        return black_num;
    }
    int getMinMax()
    {
        return white_num-black_num;
    }
    Texture &load(const int x,const int num);

};
#endif //WARCABY_PAWN_H
