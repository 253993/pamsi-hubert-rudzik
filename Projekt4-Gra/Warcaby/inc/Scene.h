//
// Created by hubert on 23.05.2021.
//

#ifndef WARCABY_SCENE_H
#define WARCABY_SCENE_H
#include "../inc/Pawn.h"
#include "../inc/Board.h"


class Scene {
    bool next_move=true;
    bool can_beat=false;
    bool isMove=false;
    bool was_beatd2=false,was_beatd1=false;
    bool color;
    std::vector<int> wektor;
    int n=0,k=0,index=-1;
    float dx=0,dy=0;
    Vector2f current_pos[24];
    Vector2i prev_vec,next_vec;
    Board board;
    Pawns pawns;
    Text text,result_white,result_black,t_rules[4],who_won[2];
    std::string results_black,results_white;
    Font font;

public:
    void TwoPlayers();
    void Player_vs_AI();
    Scene();
    bool canBeat( Vector2i pawn);

    bool canMove(Vector2i start, Vector2i end);
    bool beatMove(Vector2i start,Vector2i stop);

    bool canMoveAI(Vector2i start,Board boardx);
    bool canMoveAI_plus(Vector2i start, Vector2i end,Board boardx);
    bool canBeatAI(Vector2i start,Board boardx);
    bool canBeatAI_plus(Vector2i start, Vector2i stop,Board boardx);

    std::pair <int,Board> MinMax(int depth,std::pair <int,Board> position,bool isMax);
    std::vector<std::pair<bool,Board>> possibleMoves(Board boardx);
};

#endif //WARCABY_SCENE_H
