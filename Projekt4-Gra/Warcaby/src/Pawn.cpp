//
// Created by hubert on 23.05.2021.
//
#include "../inc/Pawn.h"

Texture& Pawns::load(int x,int num)  {
    if(x==1)
    {
        t_pawns[num].loadFromFile("../images/biały.png");
        return t_pawns[num];
    }
    else
    {
        t_pawns [num].loadFromFile("../images/czarny.png");
        return t_pawns[num];
    }
}