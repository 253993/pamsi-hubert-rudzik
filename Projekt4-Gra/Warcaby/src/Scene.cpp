//
// Created by hubert on 23.05.2021.
//
#include "../inc/Scene.h"
/*
 * Inicjalizajca prawej storny z wynikami
 */
Scene::Scene() {

    text.setString("Nastepny ruch: Bialy");
    font.loadFromFile("../fonts/OpenSans-Bold.ttf");
    result_white.setFont(font);
    result_black.setFont(font);
    text.setFont(font);
    text.setFillColor(Color::White);
    result_white.setFillColor(Color::White);
    result_black.setFillColor(Color::White);



    text.setPosition(800,0);
    result_white.setPosition(800,50);
    result_black.setPosition(800,100);


    results_white=std::to_string(pawns.getWhiteNum());
    results_black=std::to_string(pawns.getBlackNum());
    result_white.setString("Ilosc Bialych: "+results_white);
    result_black.setString("Ilosc Czarnych: "+results_black);
    int i=0;
    for(auto &x: t_rules) {
        x.setFont(font);
        x.setFillColor(Color::White);
        x.setPosition(800,500+i*40);
        i++;
    }
    t_rules[0].setString("Zasady:");
    t_rules[1].setString("-Bicia obowiazkowe.");
    t_rules[2].setString("-Mozliwe jest bicie do tylu.");
    t_rules[3].setString("-Brak limitu czasowego.");

    for(auto &x: who_won) {
        x.setFont(font);
        x.setFillColor(Color::Red);
        x.setPosition(800,300);
    }
    who_won[0].setString("BIALE WYGRALY!!!");
    who_won[1].setString("CZARNE WYGRALY!!!");
}
void Scene::TwoPlayers() {

    RenderWindow window(VideoMode(1200, 800), "Warcaby", Style::Close | Style::Resize);
    Sprite b(board.load());
    Sprite p[24];
    for(int i=0;i<8;i++)
        for(int j=0;j<8;j++)
        {
            if(board.getBoard(i,j)==1) {
                p[k].setTexture(pawns.load(1, k));
                p[k].setTextureRect(IntRect(-23,-23,sizeb,sizeb));
                p[k].setPosition(sizeb*j,sizeb*i);
                k++;
            }

            if(board.getBoard(i,j)==-1) {
                p[k].setTexture(pawns.load(-1, k));
                p[k].setTextureRect(IntRect(-23,-23,sizeb,sizeb));
                p[k].setPosition(sizeb*j,sizeb*i);
                k++;
            }
        }
    start:
    /*
     * zainicjalizowanie zmiennych przechowujących początkowe pozycje pionków
     */
    for (int i = 0; i < 24; i++) {
        current_pos[i].x = p[i].getPosition().x;
        current_pos[i].y = p[i].getPosition().y;
    }
    while (window.isOpen()) {
        Vector2i pos = Mouse::getPosition(window);
        Event e;
        if (!next_move)
            text.setString("Nastepny ruch: Czarny");
        else
            text.setString("Nastepny ruch: Bialy");
        /*
         * Sprawdzenie czy w tym ruchu możliwe będzie bicie
         */
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (next_move == true)
                    if (board.getBoard(i, j) == 1||board.getBoard(i, j) == 2)
                        if (canBeat( Vector2i(i, j)))
                            can_beat = true;
                if (next_move == false)
                    if (board.getBoard(i, j) == -1|| board.getBoard(i, j) == -2)
                        if (canBeat( Vector2i(i, j)))
                            can_beat = true;
            }
        }
        /*
         * Proces podnoszenia i uposzczania pinków wraz ze zmianą ich pozycji
         */
        while (window.pollEvent(e)) {
            if (e.type == Event::Closed)
                window.close();
            /*
             * Ustalanie który pinek został podniesiony
             */

            if (e.type == Event::MouseButtonPressed)
                if (e.mouseButton.button == Mouse::Left){
                    for (int i = 0; i < 24; i++)
                        if (p[i].getGlobalBounds().contains(pos.x, pos.y)) {
                            n = i;
                            if (board.getBoard((int) pos.y / sizeb, (int) pos.x / sizeb) == 1 ||
                                board.getBoard((int) pos.y / sizeb, (int) pos.x / sizeb) == 2) {
                                color = true;
                            } else if (board.getBoard((int) pos.y / sizeb, (int) pos.x / sizeb) == -1 ||
                                       board.getBoard((int) pos.y / sizeb, (int) pos.x / sizeb) == -2) {
                                color = false;
                            }
                            if (isMove == false) {
                                current_pos[n].x = p[n].getPosition().x;
                                current_pos[n].y = p[n].getPosition().y;
                            }
                            isMove = true;
                            dx = pos.x - p[i].getPosition().x;
                            dy = pos.y - p[i].getPosition().y;
                        }
                if(board.getBoard((int)pos.y/sizeb,(int)pos.x/sizeb)==0)
                    goto start;
            }
            /*
             * ustalanie czy pinek może być postawiony tam gdzie został
             */
            if (e.type == Event::MouseButtonReleased)
                if (e.mouseButton.button == Mouse::Left) {
                    isMove = false;
                    Vector2f vec = p[n].getPosition() + Vector2f(sizeb / 2, sizeb / 2);
                    Vector2f newPos = Vector2f(sizeb * int(vec.x / sizeb), sizeb * int(vec.y / sizeb));
                    prev_vec=Vector2i ((int)current_pos[n].y/sizeb,(int)current_pos[n].x/sizeb);
                    next_vec=Vector2i ((int)pos.y/sizeb,(int)pos.x/sizeb);
                    /*
                     * Dla przypadku kiedy możliwe jest bicie musi zostać wykonane bo jest obowiązkowe
                     */
                    if (can_beat) {
                        if (beatMove(prev_vec,next_vec) &&color == true && color == next_move) {
                            p[n].setPosition(newPos);
                            if(board.getBoard(prev_vec.x,prev_vec.y)==1)
                                board.setBoard(next_vec.x, next_vec.y, 1);
                            if(board.getBoard(prev_vec.x,prev_vec.y)==2)
                                board.setBoard(next_vec.x, next_vec.y, 2);
                            board.setBoard(prev_vec.x, prev_vec.y, 0);
                            /*
                             * Zwyły pionek
                             */
                            if(board.getBoard(next_vec.x,next_vec.y)==1) {
                                board.setBoard((prev_vec.x + next_vec.x) / 2, (prev_vec.y + next_vec.y) / 2, 0);
                                for (int i = 0; i < 24; i++)
                                    if ((int) p[i].getPosition().y / sizeb == (prev_vec.x + next_vec.x) / 2 &&
                                        (int) p[i].getPosition().x / sizeb == (prev_vec.y + next_vec.y) / 2)
                                        p[i].setPosition(-100, -100);
                            }
                            /*
                             * Damka
                             */
                            else if(board.getBoard(next_vec.x,next_vec.y)==2)
                            {
                                Vector2i num;
                                if(prev_vec.x>next_vec.x && prev_vec.y>next_vec.y) {
                                    board.setBoard(next_vec.x + 1, next_vec.y + 1, 0);
                                    num.x=next_vec.x+1; num.y=next_vec.y+1;
                                }
                                if(prev_vec.x<next_vec.x && prev_vec.y<next_vec.y) {
                                    board.setBoard(next_vec.x - 1, next_vec.y - 1, 0);
                                    num.x=next_vec.x-1; num.y=next_vec.y-1;
                                }
                                if(prev_vec.x>next_vec.x && prev_vec.y<next_vec.y) {
                                    board.setBoard(next_vec.x + 1, next_vec.y - 1, 0);
                                    num.x=next_vec.x+1; num.y=next_vec.y-1;
                                }
                                if(prev_vec.x<next_vec.x && prev_vec.y>next_vec.y) {
                                    board.setBoard(next_vec.x - 1, next_vec.y + 1, 0);
                                    num.x=next_vec.x-1; num.y=next_vec.y+1;
                                }
                                for (int i = 0; i < 24; i++) {
                                    if ((int) p[i].getPosition().y / sizeb == num.x &&
                                        (int) p[i].getPosition().x / sizeb == num.y)
                                        p[i].setPosition(-100, -100);
                                }
                            }
                            pawns.setBlackNum();
                            results_black = std::to_string(pawns.getBlackNum());
                            result_black.setString("Ilosc Czarnych: " + results_black);
                            /*
                             * Czy damka
                             */
                            if(next_vec.x==0&&board.getBoard(next_vec.x,next_vec.y)!=2) {
                                pawns.setTexture(n, "../images/BiałaDamka.png");
                                board.setBoard(next_vec.x,next_vec.y,2);
                            }
                            can_beat=false;
                            if (canBeat(next_vec)) {
                                        can_beat = true;
                                    }
                            if(can_beat)
                                next_move=true;
                            else
                                next_move=false;

                        }else if (beatMove( prev_vec,next_vec) &&color == false && color == next_move) {
                                        p[n].setPosition(newPos);
                            if(board.getBoard(prev_vec.x,prev_vec.y)==-1)
                                board.setBoard(next_vec.x, next_vec.y, -1);
                            if(board.getBoard(prev_vec.x,prev_vec.y)==-2)
                                board.setBoard(next_vec.x, next_vec.y, -2);
                            board.setBoard(prev_vec.x, prev_vec.y, 0);
                            if(board.getBoard(next_vec.x,next_vec.y)==-1) {
                                board.setBoard((prev_vec.x + next_vec.x) / 2,(prev_vec.y + next_vec.y) / 2, 0);
                                for (int i = 0; i < 24; i++)
                                    if ((int) p[i].getPosition().y / sizeb == (prev_vec.x + next_vec.x) / 2 &&
                                        (int) p[i].getPosition().x / sizeb == (prev_vec.y + next_vec.y) / 2)
                                        p[i].setPosition(-100, -100);
                            }
                            else if(board.getBoard(next_vec.x,next_vec.y)==-2)
                            {
                                Vector2i num;
                                if(prev_vec.x>next_vec.x && prev_vec.y>next_vec.y) {
                                    board.setBoard(next_vec.x + 1, next_vec.y + 1, 0);
                                    num.x=next_vec.x+1; num.y=next_vec.y+1;
                                }
                                if(prev_vec.x<next_vec.x && prev_vec.y<next_vec.y) {
                                    board.setBoard(next_vec.x - 1, next_vec.y - 1, 0);
                                    num.x=next_vec.x-1; num.y=next_vec.y-1;
                                }
                                if(prev_vec.x>next_vec.x && prev_vec.y<next_vec.y) {
                                    board.setBoard(next_vec.x + 1, next_vec.y - 1, 0);
                                    num.x=next_vec.x+1; num.y=next_vec.y-1;
                                }
                                if(prev_vec.x<next_vec.x && prev_vec.y>next_vec.y) {
                                    board.setBoard(next_vec.x - 1, next_vec.y + 1, 0);
                                    num.x=next_vec.x-1; num.y=next_vec.y+1;
                                }
                                for (int i = 0; i < 24; i++){
                                    if ((int) p[i].getPosition().y / sizeb == num.x &&
                                        (int) p[i].getPosition().x / sizeb == num.y)
                                        p[i].setPosition(-100, -100);
                            }
                            }
                            pawns.setWhiteNum();
                            results_white = std::to_string(pawns.getWhiteNum());
                            result_white.setString("Ilosc Bialych: " + results_white);
                            /*
                             * Czy damka
                             */
                            if(next_vec.x==7&&board.getBoard(next_vec.x,next_vec.y)!=-2) {
                                pawns.setTexture(n, "../images/CzarnaDamka.png");
                                board.setBoard(next_vec.x,next_vec.y,-2);
                            }
                            can_beat=false;
                            if (canBeat(next_vec))
                                can_beat = true;
                            if(can_beat)
                                next_move=false;
                            else
                                next_move=true;
                        } else
                            p[n].setPosition(current_pos[n]);
                            can_beat = 0;
                        }
                            /*
                             * Jeśli nie ma bicia któyś z pionów musi ruszyć się o 1 ruch po skosie do przodu
                             */
                        else {
                            if (canMove(prev_vec,next_vec) &&color == false && color == next_move) {
                                p[n].setPosition(newPos);
                                if(board.getBoard(prev_vec.x,prev_vec.y)==-1)
                                board.setBoard(next_vec.x, next_vec.y, -1);
                                else board.setBoard(next_vec.x, next_vec.y, -2);
                                board.setBoard(prev_vec.x, prev_vec.y, 0);
                                if(next_vec.x==7&&board.getBoard(next_vec.x,next_vec.y)!=-2) {
                                    pawns.setTexture(n, "../images/CzarnaDamka.png");
                                    board.setBoard(next_vec.x,next_vec.y,-2);
                                }
                                next_move = true;
                            } else if (canMove( prev_vec,next_vec) &&color == true && color == next_move) {
                                p[n].setPosition(newPos);
                                if(board.getBoard(prev_vec.x,prev_vec.y)==1)
                                board.setBoard(next_vec.x, next_vec.y, 1);
                                else board.setBoard(next_vec.x, next_vec.y, 2);
                                board.setBoard(prev_vec.x, prev_vec.y, 0);
                                if (next_vec.x == 0 && board.getBoard(next_vec.x, next_vec.y) != 2) {
                                    pawns.setTexture(n, "../images/BiałaDamka.png");
                                    board.setBoard(next_vec.x, next_vec.y, 2);
                                }
                                next_move = false;
                            }else
                                p[n].setPosition(current_pos[n]);
                            can_beat = 0;
                        }
                    }
                }
            /*
             * Kiedy ruch został zakończony pion przesuwa się na dokładniej ustalone pole
             */
            if (isMove) {
                p[n].setPosition(pos.x - dx, pos.y - dy);
            }
            /*
             * Naniesienie wszystkich elementów na ekran
             */
            window.clear(Color::Transparent);
            window.draw(b);
            for (int i = 0; i < 24; i++) { window.draw(p[i]); }
            window.draw(text);
            window.draw(result_white);
            window.draw(result_black);
            window.draw(t_rules[0]);
            window.draw(t_rules[1]);
            window.draw(t_rules[2]);
            window.draw(t_rules[3]);
        if (pawns.getWhiteNum() == 0)
                window.draw(who_won[1]);
            if (pawns.getBlackNum() == 0)
                window.draw(who_won[0]);
            window.display();
        }
    }
void Scene::Player_vs_AI() {

    RenderWindow window(VideoMode(1200, 800), "Warcaby", Style::Close | Style::Resize);
    Sprite b(board.load());
    Sprite p[24];
    for(int i=0;i<8;i++)
        for(int j=0;j<8;j++)
        {
            if(board.getBoard(i,j)==1) {
                p[k].setTexture(pawns.load(1, k));
                p[k].setTextureRect(IntRect(-23,-23,sizeb,sizeb));
                p[k].setPosition(sizeb*j,sizeb*i);
                k++;
            }
            if(board.getBoard(i,j)==-1) {
                p[k].setTexture(pawns.load(-1, k));
                p[k].setTextureRect(IntRect(-23,-23,sizeb,sizeb));
                p[k].setPosition(sizeb*j,sizeb*i);
                k++;
            }
        }
    start:
    /*
     * zainicjalizowanie zmiennych przechowujących początkowe pozycje pionków
     */
    for (int i = 0; i < 24; i++) {
        current_pos[i].x = p[i].getPosition().x;
        current_pos[i].y = p[i].getPosition().y;
    }
    while (window.isOpen()) {
        Vector2i pos = Mouse::getPosition(window);
        Event e;
        if (!next_move)
            text.setString("Nastepny ruch: Czarny");
        else
            text.setString("Nastepny ruch: Bialy");
        /*
         * Sprawdzenie czy w tym ruchu możliwe będzie bicie
         */
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (next_move == true)
                    if (board.getBoard(i, j) == 1 || board.getBoard(i, j) == 2)
                        if (canBeat( Vector2i(i, j)))
                            can_beat = true;
                if (next_move == false)
                    if (board.getBoard(i, j) == -1 || board.getBoard(i, j) == -2)
                        if (canBeat(Vector2i(i, j)))
                            can_beat = true;
            }
        }
        /*
         * Proces podnoszenia i uposzczania pinków wraz ze zmianą ich pozycji
         */
        while (window.pollEvent(e)) {
            if (e.type == Event::Closed)
                window.close();
            /*
             * Dla gracza
             */
            if(next_move==true) {
                if (e.type == Event::MouseButtonPressed)
                    if (e.mouseButton.button == Mouse::Left) {
                        for (int i = 0; i < 24; i++)
                            if (p[i].getGlobalBounds().contains(pos.x, pos.y)) {
                                n = i;
                                if (board.getBoard((int) pos.y / sizeb, (int) pos.x / sizeb) == 1 ||
                                    board.getBoard((int) pos.y / sizeb, (int) pos.x / sizeb) == 2) {
                                    color = true;
                                } else if (board.getBoard((int) pos.y / sizeb, (int) pos.x / sizeb) == -1 ||
                                           board.getBoard((int) pos.y / sizeb, (int) pos.x / sizeb) == -2) {
                                    color = false;
                                }
                                if (isMove == false) {
                                    current_pos[n].x = p[n].getPosition().x;
                                    current_pos[n].y = p[n].getPosition().y;
                                }
                                isMove = true;
                                dx = pos.x - p[i].getPosition().x;
                                dy = pos.y - p[i].getPosition().y;
                            }
                        if (board.getBoard((int) pos.y / sizeb, (int) pos.x / sizeb) == 0)
                            goto start;
                    }
                /*
                 * ustalanie czy pinek może być postawiony tam gdzie został
                 */
                if (e.type == Event::MouseButtonReleased)
                    if (e.mouseButton.button == Mouse::Left) {
                        wektor.clear();
                        isMove = false;
                        Vector2f vec = p[n].getPosition() + Vector2f(sizeb / 2, sizeb / 2);
                        Vector2f newPos = Vector2f(sizeb * int(vec.x / sizeb), sizeb * int(vec.y / sizeb));
                        prev_vec = Vector2i((int) current_pos[n].y / sizeb, (int) current_pos[n].x / sizeb);
                        next_vec = Vector2i((int) pos.y / sizeb, (int) pos.x / sizeb);
                        /*
                         * Dla przypadku kiedy możliwe jest bicie musi zostać wykonane bo jest obowiązkowe
                         */
                        if (color == true) {
                            if (can_beat) {
                                if (beatMove(prev_vec, next_vec) && color == true &&
                                    color == next_move) {
                                    p[n].setPosition(newPos);
                                    if (board.getBoard(prev_vec.x, prev_vec.y) == 1)
                                        board.setBoard(next_vec.x, next_vec.y, 1);
                                    if (board.getBoard(prev_vec.x, prev_vec.y) == 2)
                                        board.setBoard(next_vec.x, next_vec.y, 2);
                                    board.setBoard(prev_vec.x, prev_vec.y, 0);
                                    /*
                                     * Zwyły pionek
                                     */
                                    if (board.getBoard(next_vec.x, next_vec.y) == 1) {
                                        board.setBoard((prev_vec.x + next_vec.x) / 2, (prev_vec.y + next_vec.y) / 2, 0);
                                        for (int i = 0; i < 24; i++)
                                            if ((int) p[i].getPosition().y / sizeb == (prev_vec.x + next_vec.x) / 2 &&
                                                (int) p[i].getPosition().x / sizeb == (prev_vec.y + next_vec.y) / 2)
                                                p[i].setPosition(-1000, -1000);
                                    }
                                        /*
                                         * Damka
                                         */
                                    else if (board.getBoard(next_vec.x, next_vec.y) == 2) {
                                        Vector2i num;
                                        if (prev_vec.x > next_vec.x && prev_vec.y > next_vec.y) {
                                            board.setBoard(next_vec.x + 1, next_vec.y + 1, 0);
                                            num.x = next_vec.x + 1;
                                            num.y = next_vec.y + 1;
                                        }
                                        if (prev_vec.x < next_vec.x && prev_vec.y < next_vec.y) {
                                            board.setBoard(next_vec.x - 1, next_vec.y - 1, 0);
                                            num.x = next_vec.x - 1;
                                            num.y = next_vec.y - 1;
                                        }
                                        if (prev_vec.x > next_vec.x && prev_vec.y < next_vec.y) {
                                            board.setBoard(next_vec.x + 1, next_vec.y - 1, 0);
                                            num.x = next_vec.x + 1;
                                            num.y = next_vec.y - 1;
                                        }
                                        if (prev_vec.x < next_vec.x && prev_vec.y > next_vec.y) {
                                            board.setBoard(next_vec.x - 1, next_vec.y + 1, 0);
                                            num.x = next_vec.x - 1;
                                            num.y = next_vec.y + 1;
                                        }
                                        for (int i = 0; i < 24; i++) {
                                            if ((int) p[i].getPosition().y / sizeb == num.x &&
                                                (int) p[i].getPosition().x / sizeb == num.y)
                                                p[i].setPosition(-1000, -1000);
                                        }
                                    }
                                    pawns.setBlackNum();
                                    results_black = std::to_string(pawns.getBlackNum());
                                    result_black.setString("Ilosc Czarnych: " + results_black);
                                    /*
                                     * Czy damka
                                     */
                                    if (next_vec.x == 0 && board.getBoard(next_vec.x, next_vec.y) != 2) {
                                        pawns.setTexture(n, "../images/BiałaDamka.png");
                                        board.setBoard(next_vec.x, next_vec.y, 2);
                                    }
                                    can_beat = false;
                                    if (canBeat( next_vec)) {
                                        can_beat = true;
                                    }
                                    if (can_beat)
                                        next_move = true;
                                    else
                                        next_move = false;

                                } else
                                    p[n].setPosition(current_pos[n]);
                                can_beat = 0;
                            }
                                /*
                                 * Jeśli nie ma bicia któyś z pionów musi ruszyć się o 1 ruch po skosie do przodu
                                 */
                            else {
                                if (canMove(prev_vec, next_vec) && color == true &&
                                    color == next_move) {
                                    p[n].setPosition(newPos);
                                    if (board.getBoard(prev_vec.x, prev_vec.y) == 1)
                                        board.setBoard(next_vec.x, next_vec.y, 1);
                                    else board.setBoard(next_vec.x, next_vec.y, 2);
                                    board.setBoard(prev_vec.x, prev_vec.y, 0);
                                    if (next_vec.x == 0 && board.getBoard(next_vec.x, next_vec.y) != 2) {
                                        pawns.setTexture(n, "../images/BiałaDamka.png");
                                        board.setBoard(next_vec.x, next_vec.y, 2);
                                    }
                                    next_move = false;
                                } else
                                    p[n].setPosition(current_pos[n]);
                                can_beat = 0;
                            }

                        }
                    }
            }
            /*
             * Ruch AI
             */
            //TODO Zbjianie AI
            else if(!next_move) {
                color=false;
                Vector2i prev_pos;
                Vector2i next_pos;
                possibleMoves(board);
                std::pair<int, Board> para;
                para.first = 0;
                para.second = board;
                Board board2 = MinMax(2, para, false).second;
                board2.load();
                /*
                 * wybieranie przesuniętego pionka
                 */
                for(int i=0;i<8;i++)
                    for(int j=0;j<8;j++)
                        if(board.getBoard(i,j)==-1 && board2.getBoard(i,j)==0)
                        {
                            prev_pos=Vector2i (i,j);
                            for(int l=0;l<12;l++){
                                if((int)p[l].getPosition().y/sizeb==i && (int)p[l].getPosition().x/sizeb==j)
                                    index=l;
                            }
                        }
                for(int i=0;i<8;i++)
                    for(int j=0;j<8;j++) {
                        if (board.getBoard(i, j) == 0 && board2.getBoard(i, j) == -1) {
                            next_pos=Vector2i (i,j);
                            p[index].setPosition(j * sizeb, i * sizeb);
                            if(abs(prev_pos.x-next_pos.x)==2)
                            {
                                for (int o = 12; o < 24; o++)
                                    if ((int) p[o].getPosition().y / sizeb == (prev_pos.x + next_pos.x) / 2 &&
                                        (int) p[o].getPosition().x / sizeb == (prev_pos.y + next_pos.y) / 2)
                                        p[o].setPosition(-1000, -1000);
                                    board2.setBoard((prev_pos.x+next_pos.x)/2,(prev_pos.y+next_pos.y)/2,0);
                                    pawns.setWhiteNum();
                                    results_white = std::to_string(pawns.getWhiteNum());
                                    result_white.setString("Ilosc Bialych: " + results_white);
                            }

                        }
                    }

                next_move=true;
                board=board2;
            }
        }
            /*
             * Kiedy ruch został zakończony pion przesuwa się na dokładniej ustalone pole
             */
            if (isMove) {
                p[n].setPosition(pos.x - dx, pos.y - dy);
            }
            /*
             * Naniesienie wszystkich elementów na ekran
             */
            window.clear(Color::Transparent);
            window.draw(b);
            for (int i = 0; i < 24; i++) { window.draw(p[i]);}
            window.draw(text);
            window.draw(result_white);
            window.draw(result_black);
            window.draw(t_rules[0]);
            window.draw(t_rules[1]);
            window.draw(t_rules[2]);
            window.draw(t_rules[3]);
        if (pawns.getWhiteNum() == 0)
                window.draw(who_won[1]);
            if (pawns.getBlackNum() == 0)
                window.draw(who_won[0]);
            window.display();

        }
    }

