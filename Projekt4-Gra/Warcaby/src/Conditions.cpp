//
// Created by hubert on 01.06.2021.
//

#include "../inc/Scene.h"

bool Scene::canMove(Vector2i start, Vector2i end)
{
    if(board.getBoard(end.x,end.y)!=0)
        return false;
    if(board.getBoardcol(end.x,end.y)==0)
        return false;
    if(!color) {
        if (start.x + 1==end.x && (start.y+1==end.y||start.y-1==end.y) && board.getBoard(start.x,start.y)==-1)
            return true;
    }
    if(color) {
        if (start.x - 1==end.x && (start.y+1==end.y||start.y-1==end.y) && board.getBoard(start.x,start.y)==1)
            return true;
    }
    for(int i=0;i<8;i++)
    {
        if(color) {
            if (start.x + i==end.x && (start.y+i==end.y||start.y-i==end.y) && board.getBoard(start.x,start.y)==2)
                return true;
            else if (start.x - i==end.x && (start.y+i==end.y||start.y-i==end.y) && board.getBoard(start.x,start.y)==2)
                return true;
        }
        if(!color) {
            if (start.x - i==end.x && (start.y+i==end.y||start.y-i==end.y) && board.getBoard(start.x,start.y)==-2)
                return true;
            else if (start.x + i==end.x && (start.y+i==end.y||start.y-i==end.y) && board.getBoard(start.x,start.y)==-2)
                return true;
        }
    }
    return false;
}

bool Scene::canMoveAI(Vector2i start,Board boardx)
{

    if(!color) {
        if (start.x+1<8)
            if(boardx.getBoard(start.x,start.y)==-1)
                if((boardx.getBoard(start.x+1,start.y+1)==0 && start.y+1<8)
                || (boardx.getBoard(start.x+1,start.y-1)==0 && start.y-1>-1))
                    return true;
    }
    if(color) {
        if (start.x-1>-1)
            if(boardx.getBoard(start.x,start.y)==1)
                if((boardx.getBoard(start.x-1,start.y+1)==0 && start.y+1<8)
                || (boardx.getBoard(start.x-1,start.y-1)==0 && start.y-1>-1))
                    return true;
    }
    return false;
}
bool Scene::canBeat(Vector2i pawn) {

    if(color) {
        if(board.getBoard(pawn.x,pawn.y)==1) {
            if ((board.getBoard(pawn.x - 1, pawn.y - 1) == -1 || board.getBoard(pawn.x - 1, pawn.y - 1) == -2) &&
                board.getBoard(pawn.x - 2, pawn.y - 2) == 0 && pawn.x - 2 > -1 && pawn.y - 2 > -1)
                return true;
            if ((board.getBoard(pawn.x - 1, pawn.y + 1) == -1 || board.getBoard(pawn.x - 1, pawn.y + 1) == -2) &&
                board.getBoard(pawn.x - 2, pawn.y + 2) == 0 && pawn.x - 2 > -1 && pawn.y + 2 < 8)
                return true;
            if ((board.getBoard(pawn.x + 1, pawn.y - 1) == -1 || board.getBoard(pawn.x + 1, pawn.y - 1) == -2) &&
                board.getBoard(pawn.x + 2, pawn.y - 2) == 0 && pawn.x + 2 < 8 && pawn.y - 2 > -1)
                return true;
            if ((board.getBoard(pawn.x + 1, pawn.y + 1) == -1 || board.getBoard(pawn.x + 1, pawn.y + 1) == -2) &&
                board.getBoard(pawn.x + 2, pawn.y + 2) == 0 && pawn.x + 2 < 8 && pawn.y + 2 < 8)
                return true;
        }
        if(board.getBoard(pawn.x,pawn.y)==2) {
            for (int i = 1; i < 8; i++) {
                if (board.getBoard(pawn.x - i, pawn.y - i) == -1 && board.getBoard(pawn.x - i - 1, pawn.y - i - 1) == 0 &&
                    pawn.x - i - 1 > -1 && pawn.y - i - 1 > -1 && (board.getBoard(pawn.x-i+1,pawn.y-i+1)==0 || board.getBoard(pawn.x-i+1,pawn.y-i+1)==2))
                    return true;
                if (board.getBoard(pawn.x - i, pawn.y + i) == -1 && board.getBoard(pawn.x - i - 1, pawn.y + i + 1) == 0 &&
                    pawn.x - i - 1 > -1 && pawn.y + i + 1 < 8 && (board.getBoard(pawn.x-i+1,pawn.y+i-1)==0 || board.getBoard(pawn.x-i+1,pawn.y+i-1)==2))
                    return true;
                if (board.getBoard(pawn.x + i, pawn.y - i) == -1 && board.getBoard(pawn.x + i + 1, pawn.y - i - 1) == 0 &&
                    pawn.x + i + 1 < 8 && pawn.y - i - 1 > -1 &&(board.getBoard(pawn.x+i-1,pawn.y-i+1)==0 ||board.getBoard(pawn.x+i-1,pawn.y-i+1)==2))
                    return true;
                if (board.getBoard(pawn.x + i, pawn.y + i) == -1 && board.getBoard(pawn.x + i + 1, pawn.y + i + 1) == 0 &&
                    pawn.x + i + 1 < 8 && pawn.y + i + 1 < 8 &&(board.getBoard(pawn.x+i-1,pawn.y+i-1)==0|| board.getBoard(pawn.x+i-1,pawn.y+i-1)==2))
                    return true;
            }
        }
    }
    if(!color) {
        if (board.getBoard(pawn.x, pawn.y) == -1) {
            if ((board.getBoard(pawn.x - 1, pawn.y - 1) == 1 || board.getBoard(pawn.x - 1, pawn.y - 1) == 2)
                && board.getBoard(pawn.x - 2, pawn.y - 2) == 0 && pawn.x - 2 > -1 && pawn.y - 2 > -1)
                return true;
            if ((board.getBoard(pawn.x - 1, pawn.y + 1) == 1 || board.getBoard(pawn.x - 1, pawn.y + 1) == 2)
                && board.getBoard(pawn.x - 2, pawn.y + 2) == 0 && pawn.x - 2 > -1 && pawn.y + 2 < 8)
                return true;
            if ((board.getBoard(pawn.x + 1, pawn.y - 1) == 1 || board.getBoard(pawn.x + 1, pawn.y - 1) == 2)
                && board.getBoard(pawn.x + 2, pawn.y - 2) == 0 && pawn.x + 2 < 8 && pawn.y - 2 > -1)
                return true;
            if ((board.getBoard(pawn.x + 1, pawn.y + 1) == 1 || board.getBoard(pawn.x + 1, pawn.y + 1) == 2)
                && board.getBoard(pawn.x + 2, pawn.y + 2) == 0 && pawn.x + 2 < 8 && pawn.y + 2 < 8)
                return true;
        }
        if (board.getBoard(pawn.x, pawn.y) == -2) {
            for (int i = 0; i < 8; i++) {
                if (board.getBoard(pawn.x - i, pawn.y - i) == 1 && board.getBoard(pawn.x - i - 1, pawn.y - i - 1) == 0 &&
                    pawn.x - i - 1 > -1 && pawn.y - i - 1 > -1 && (board.getBoard(pawn.x-i+1,pawn.y-i+1)==0 || board.getBoard(pawn.x-i+1,pawn.y-i+1)==-2))
                    return true;
                if (board.getBoard(pawn.x + i, pawn.y + i) == 1 && board.getBoard(pawn.x + i + 1, pawn.y + i + 1) == 0 &&
                    pawn.x + i + 1 < 8 && pawn.y + i + 1 < 8&&(board.getBoard(pawn.x+i-1,pawn.y+i-1)==0|| board.getBoard(pawn.x+i-1,pawn.y+i-1)==-2))
                    return true;
                if (board.getBoard(pawn.x - i, pawn.y + i) == 1 && board.getBoard(pawn.x - i - 1, pawn.y + i + 1) == 0 &&
                    pawn.x - i - 1 > -1 && pawn.y + i + 1 < 8&& (board.getBoard(pawn.x-i+1,pawn.y+i-1)==0 || board.getBoard(pawn.x-i+1,pawn.y+i-1)==-2))
                    return true;
                if (board.getBoard(pawn.x + i, pawn.y - i) == 1 && board.getBoard(pawn.x + i + 1, pawn.y - i - 1) == 0 &&
                    pawn.x + i + 1 < 8 && pawn.y - i - 1 > -1&&(board.getBoard(pawn.x+i-1,pawn.y+i-1)==0|| board.getBoard(pawn.x+i-1,pawn.y+i-1)==-2))
                    return true;
            }
        }
    }
    return false;
}
bool Scene::beatMove(Vector2i start,Vector2i stop)
{
    if(color) {
        if (board.getBoard(start.x, start.y) == 1) {
            if (board.getBoard(start.x - 1, start.y - 1) == -1 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y - 2) == stop)
                return true;
            if (board.getBoard(start.x - 1, start.y + 1) == -1 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y + 2) == stop)
                return true;
            if (board.getBoard(start.x + 1, start.y - 1) == -1 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y - 2) == stop)
                return true;
            if (board.getBoard(start.x + 1, start.y + 1) == -1 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y + 2) == stop)
                return true;
            if (board.getBoard(start.x - 1, start.y - 1) == -2 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y - 2) == stop)
                return true;
            if (board.getBoard(start.x - 1, start.y + 1) == -2 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y + 2) == stop)
                return true;
            if (board.getBoard(start.x + 1, start.y - 1) == -2 && board.getBoard(stop.x, stop.y) == 0 &&Vector2i(start.x + 2, start.y - 2) == stop)
                return true;
            if (board.getBoard(start.x + 1, start.y + 1) == -2 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y + 2) == stop)
                return true;
        }
        if(board.getBoard(start.x,start.y)==2)
        {
            for(int i=0;i<8;i++)
            {
                if (board.getBoard(start.x - i, start.y - i) == -1 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y - i-1) == stop)
                    return true;
                if (board.getBoard(start.x - i, start.y + i) == -1 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y + i+1) == stop)
                    return true;
                if (board.getBoard(start.x + i, start.y - i) == -1 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y - i-1) == stop)
                    return true;
                if (board.getBoard(start.x + i, start.y + i) == -1 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y + i+1) == stop)
                    return true;
                if (board.getBoard(start.x - i, start.y - i) == -2 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y - i-1) == stop)
                    return true;
                if (board.getBoard(start.x - i, start.y + i) == -2 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y + i+1) == stop)
                    return true;
                if (board.getBoard(start.x + i, start.y - i) == -2 && board.getBoard(stop.x, stop.y) == 0&&
                    Vector2i(start.x + i+1, start.y - i-1) == stop)
                    return true;
                if (board.getBoard(start.x + i, start.y + i) == -2 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y + i+1) == stop)
                    return true;

            }
        }

    }
    if(!color) {
        if (board.getBoard(start.x, start.y) == -1) {
            if (board.getBoard(start.x - 1, start.y - 1) == 1 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y - 2) == stop)
                return true;
            if (board.getBoard(start.x - 1, start.y + 1) == 1 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y + 2) == stop)
                return true;
            if (board.getBoard(start.x + 1, start.y - 1) == 1 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y - 2) == stop)
                return true;
            if (board.getBoard(start.x + 1, start.y + 1) == 1 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y + 2) == stop)
                return true;
            if (board.getBoard(start.x - 1, start.y - 1) == 2 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y - 2) == stop)
                return true;
            if (board.getBoard(start.x - 1, start.y + 1) == 2 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y + 2) == stop)
                return true;
            if (board.getBoard(start.x + 1, start.y - 1) == 2 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y - 2) == stop)
                return true;
            if (board.getBoard(start.x + 1, start.y + 1) == 2 && board.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y + 2) == stop)
                return true;
        }
        if (board.getBoard(start.x, start.y) == -2) {
            for(int i=0;i<8;i++)
            {
                if (board.getBoard(start.x - i, start.y - i) == 1 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y - i-1) == stop)
                    return true;
                if (board.getBoard(start.x - i, start.y + i) == 1 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y + i+1) == stop)
                    return true;
                if (board.getBoard(start.x + i, start.y - i) == 1 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y - i-1) == stop)
                    return true;
                if (board.getBoard(start.x + i, start.y + i) == 1 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y + i+1) == stop)
                    return true;
                if (board.getBoard(start.x - i, start.y - i) == 2 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y - i-1) == stop)
                    return true;
                if (board.getBoard(start.x - i, start.y + i) == 2 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y + i+1) == stop)
                    return true;
                if (board.getBoard(start.x + i, start.y - i) == 2 && board.getBoard(stop.x, stop.y) == 0&&
                    Vector2i(start.x + i+1, start.y - i-1) == stop)
                    return true;
                if (board.getBoard(start.x + i, start.y + i) == 2 && board.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y + i+1) == stop)
                    return true;
            }
        }
    }
    return false;
}

std::pair <int,Board > Scene::MinMax(int depth,std::pair <int,Board> position,bool isMax) {
        if (depth==0 ){
            position.first=pawns.getMinMax();
            wektor.push_back(position.first);
            return  position;
        }
        if(isMax) {
            color=true;
            std::pair<int, Board> maxEval;
            maxEval.first = -INT_MAX;
            std::vector<std::pair<bool,Board>> move;
            for (int i = 0; i < possibleMoves(position.second).size(); i++) {
                move.push_back(possibleMoves(position.second)[i]);
            }
            for (int i = 0; i < move.size(); i++) {
                std::pair<int,Board> pair;
                pair.second=move[i].second;
                if(move[i].first){
                    pawns.setBlackNum();
                    was_beatd1=true;
                }
                std::pair<int,Board> eval= MinMax(depth-1,pair,false);
                color=true;
                if(was_beatd1&&depth==1) {
                    pawns.setBlackNum_plus();
                    was_beatd1=false;
                }
                if(maxEval.first<eval.first) {
                    maxEval.first = eval.first;
                    maxEval.second = position.second;
                }
            }
            return maxEval;
        }
        else{
            color=false;
            std::pair<int, Board> minEval;
            minEval.first = INT_MAX;
            std::vector<std::pair<bool,Board>> move;
            for (int i = 0; i < possibleMoves(position.second).size(); i++) {
                move.push_back(possibleMoves(position.second)[i]);
            }
            for (int i = 0; i < move.size(); i++) {
                std::pair<int,Board> pair;
                pair.second=move[i].second;
                if(move[i].first){
                    was_beatd2=true;
                    pawns.setWhiteNum();
                }
                std::pair<int,Board> eval= MinMax(depth-1,pair,true);
                color=false;
                if(was_beatd2) {
                    pawns.setWhiteNum_plus();
                    was_beatd2=false;
                }
                if(minEval.first>eval.first) {
                    minEval.first = eval.first;
                    minEval.second = eval.second;
                }
            }
            return minEval;
    }
}
std::vector<std::pair<bool,Board>> Scene::possibleMoves(Board boardx){
    Board vector;
    std::pair<bool,Board> pair;
    std::vector<std::pair<bool,Board>> move;
    if(color)
    {
        for(int i=0;i<8;i++)
            for(int j=0;j<8;j++)
                if (canBeatAI( Vector2i(i, j),boardx))
                    for(int h=-2;h<3;h++)
                        for(int d=-2;d<3;d++)
                            if(canBeatAI_plus(Vector2i(i, j),Vector2i(i+h, j+d),boardx)){
                                Board new_board;
                                Board copy=boardx;
                                new_board=copy;
                                new_board.setBoard(i,j,0);
                                new_board.setBoard(i+h,j+d,1);
                                pair.first=true;
                                pair.second=new_board;
                                move.push_back(pair);
                            }
        for(int i=0;i<8;i++)
            for(int j=0;j<8;j++)
                if (canMoveAI( Vector2i(i, j),boardx))
                    for(int h=-1;h<2;h++)
                        for(int d=-1;d<2;d++)
                            if(canMoveAI_plus(Vector2i(i, j),Vector2i(i+h, j+d),boardx)) {
                                Board new_board;
                                Board copy=boardx;
                                new_board=copy;
                                new_board.setBoard(i,j,0);
                                new_board.setBoard(i+h,j+d,1);
                                pair.first=false;
                                pair.second=new_board;
                                move.push_back(pair);
                            }
    }
    else{
        for(int i=0;i<8;i++)
            for(int j=0;j<8;j++)
                if (canBeatAI( Vector2i(i, j),boardx))
                    for(int h=-2;h<3;h++)
                        for(int d=-2;d<3;d++)
                            if(canBeatAI_plus(Vector2i(i, j),Vector2i(i+h, j+d),boardx)){
                                Board new_board;
                                Board copy=boardx;
                                new_board=copy;
                                new_board.setBoard(i,j,0);
                                new_board.setBoard(i+h,j+d,-1);
                                pair.first=true;
                                pair.second=new_board;
                                move.push_back(pair);
                            }
        for(int i=0;i<8;i++)
            for(int j=0;j<8;j++)
                if (canMoveAI( Vector2i(i, j),boardx))
                    for(int h=-1;h<2;h++)
                        for(int d=-1;d<2;d++)
                            if(canMoveAI_plus(Vector2i(i, j),Vector2i(i+h, j+d),boardx)) {
                                Board new_board;
                                Board copy=boardx;
                                new_board=copy;
                                new_board.setBoard(i,j,0);
                                new_board.setBoard(i+h,j+d,-1);
                                pair.first=false;
                                pair.second=new_board;
                                move.push_back(pair);
                            }
    }
    return move;
}
bool Scene::canBeatAI(Vector2i pawn,Board boardx) {

    if(color) {
        if(boardx.getBoard(pawn.x,pawn.y)==1) {
            if ((boardx.getBoard(pawn.x - 1, pawn.y - 1) == -1 || boardx.getBoard(pawn.x - 1, pawn.y - 1) == -2) &&
                boardx.getBoard(pawn.x - 2, pawn.y - 2) == 0 && pawn.x - 2 > -1 && pawn.y - 2 > -1)
                return true;
            if ((boardx.getBoard(pawn.x - 1, pawn.y + 1) == -1 || boardx.getBoard(pawn.x - 1, pawn.y + 1) == -2) &&
                boardx.getBoard(pawn.x - 2, pawn.y + 2) == 0 && pawn.x - 2 > -1 && pawn.y + 2 < 8)
                return true;
            if ((boardx.getBoard(pawn.x + 1, pawn.y - 1) == -1 || boardx.getBoard(pawn.x + 1, pawn.y - 1) == -2) &&
                boardx.getBoard(pawn.x + 2, pawn.y - 2) == 0 && pawn.x + 2 < 8 && pawn.y - 2 > -1)
                return true;
            if ((boardx.getBoard(pawn.x + 1, pawn.y + 1) == -1 || boardx.getBoard(pawn.x + 1, pawn.y + 1) == -2) &&
                boardx.getBoard(pawn.x + 2, pawn.y + 2) == 0 && pawn.x + 2 < 8 && pawn.y + 2 < 8)
                return true;
        }
        if(boardx.getBoard(pawn.x,pawn.y)==2) {
            for (int i = 1; i < 8; i++) {
                if (boardx.getBoard(pawn.x - i, pawn.y - i) == -1 && boardx.getBoard(pawn.x - i - 1, pawn.y - i - 1) == 0 &&
                    pawn.x - i - 1 > -1 && pawn.y - i - 1 > -1 && (boardx.getBoard(pawn.x-i+1,pawn.y-i+1)==0 || boardx.getBoard(pawn.x-i+1,pawn.y-i+1)==2))
                    return true;
                if (boardx.getBoard(pawn.x - i, pawn.y + i) == -1 && boardx.getBoard(pawn.x - i - 1, pawn.y + i + 1) == 0 &&
                    pawn.x - i - 1 > -1 && pawn.y + i + 1 < 8 && (boardx.getBoard(pawn.x-i+1,pawn.y+i-1)==0 || boardx.getBoard(pawn.x-i+1,pawn.y+i-1)==2))
                    return true;
                if (boardx.getBoard(pawn.x + i, pawn.y - i) == -1 && boardx.getBoard(pawn.x + i + 1, pawn.y - i - 1) == 0 &&
                    pawn.x + i + 1 < 8 && pawn.y - i - 1 > -1 &&(boardx.getBoard(pawn.x+i-1,pawn.y-i+1)==0 ||boardx.getBoard(pawn.x+i-1,pawn.y-i+1)==2))
                    return true;
                if (boardx.getBoard(pawn.x + i, pawn.y + i) == -1 && boardx.getBoard(pawn.x + i + 1, pawn.y + i + 1) == 0 &&
                    pawn.x + i + 1 < 8 && pawn.y + i + 1 < 8 &&(boardx.getBoard(pawn.x+i-1,pawn.y+i-1)==0|| boardx.getBoard(pawn.x+i-1,pawn.y+i-1)==2))
                    return true;
            }
        }
    }
    if(!color) {
        if (boardx.getBoard(pawn.x, pawn.y) == -1) {
            if ((boardx.getBoard(pawn.x - 1, pawn.y - 1) == 1 || boardx.getBoard(pawn.x - 1, pawn.y - 1) == 2)
                && boardx.getBoard(pawn.x - 2, pawn.y - 2) == 0 && pawn.x - 2 > -1 && pawn.y - 2 > -1)
                return true;
            if ((boardx.getBoard(pawn.x - 1, pawn.y + 1) == 1 || boardx.getBoard(pawn.x - 1, pawn.y + 1) == 2)
                && boardx.getBoard(pawn.x - 2, pawn.y + 2) == 0 && pawn.x - 2 > -1 && pawn.y + 2 < 8)
                return true;
            if ((boardx.getBoard(pawn.x + 1, pawn.y - 1) == 1 || boardx.getBoard(pawn.x + 1, pawn.y - 1) == 2)
                && boardx.getBoard(pawn.x + 2, pawn.y - 2) == 0 && pawn.x + 2 < 8 && pawn.y - 2 > -1)
                return true;
            if ((boardx.getBoard(pawn.x + 1, pawn.y + 1) == 1 || boardx.getBoard(pawn.x + 1, pawn.y + 1) == 2)
                && boardx.getBoard(pawn.x + 2, pawn.y + 2) == 0 && pawn.x + 2 < 8 && pawn.y + 2 < 8)
                return true;
        }
        if (boardx.getBoard(pawn.x, pawn.y) == -2) {
            for (int i = 0; i < 8; i++) {
                if (boardx.getBoard(pawn.x - i, pawn.y - i) == 1 && boardx.getBoard(pawn.x - i - 1, pawn.y - i - 1) == 0 &&
                    pawn.x - i - 1 > -1 && pawn.y - i - 1 > -1 && (boardx.getBoard(pawn.x-i+1,pawn.y-i+1)==0 || boardx.getBoard(pawn.x-i+1,pawn.y-i+1)==-2))
                    return true;
                if (boardx.getBoard(pawn.x + i, pawn.y + i) == 1 && boardx.getBoard(pawn.x + i + 1, pawn.y + i + 1) == 0 &&
                    pawn.x + i + 1 < 8 && pawn.y + i + 1 < 8&&(boardx.getBoard(pawn.x+i-1,pawn.y+i-1)==0|| boardx.getBoard(pawn.x+i-1,pawn.y+i-1)==-2))
                    return true;
                if (boardx.getBoard(pawn.x - i, pawn.y + i) == 1 && boardx.getBoard(pawn.x - i - 1, pawn.y + i + 1) == 0 &&
                    pawn.x - i - 1 > -1 && pawn.y + i + 1 < 8&& (boardx.getBoard(pawn.x-i+1,pawn.y+i-1)==0 || boardx.getBoard(pawn.x-i+1,pawn.y+i-1)==-2))
                    return true;
                if (boardx.getBoard(pawn.x + i, pawn.y - i) == 1 && boardx.getBoard(pawn.x + i + 1, pawn.y - i - 1) == 0 &&
                    pawn.x + i + 1 < 8 && pawn.y - i - 1 > -1&&(boardx.getBoard(pawn.x+i-1,pawn.y+i-1)==0|| boardx.getBoard(pawn.x+i-1,pawn.y+i-1)==-2))
                    return true;
            }
        }
    }
    return false;
}
bool Scene::canMoveAI_plus(Vector2i start, Vector2i end,Board boardx)
{
    if(boardx.getBoard(end.x,end.y)!=0)
        return false;
    if(boardx.getBoardcol(end.x,end.y)==0)
        return false;
    if(!color) {
        if (start.x + 1==end.x && (start.y+1==end.y||start.y-1==end.y) && boardx.getBoard(start.x,start.y)==-1)
            return true;
    }
    if(color) {
        if (start.x - 1==end.x && (start.y+1==end.y||start.y-1==end.y) && boardx.getBoard(start.x,start.y)==1)
            return true;
    }
    for(int i=0;i<8;i++)
    {
        if(color) {
            if (start.x + i==end.x && (start.y+i==end.y||start.y-i==end.y) && boardx.getBoard(start.x,start.y)==2)
                return true;
            else if (start.x - i==end.x && (start.y+i==end.y||start.y-i==end.y) && boardx.getBoard(start.x,start.y)==2)
                return true;
        }
        if(!color) {
            if (start.x - i==end.x && (start.y+i==end.y||start.y-i==end.y) && boardx.getBoard(start.x,start.y)==-2)
                return true;
            else if (start.x + i==end.x && (start.y+i==end.y||start.y-i==end.y) && boardx.getBoard(start.x,start.y)==-2)
                return true;
        }
    }
    return false;
}
bool Scene::canBeatAI_plus(Vector2i start, Vector2i stop,Board boardx)
{
    if(color) {
        if (boardx.getBoard(start.x, start.y) == 1) {
            if (boardx.getBoard(start.x - 1, start.y - 1) == -1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y - 2) == stop)
                return true;
            if (boardx.getBoard(start.x - 1, start.y + 1) == -1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y + 2) == stop)
                return true;
            if (boardx.getBoard(start.x + 1, start.y - 1) == -1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y - 2) == stop)
                return true;
            if (boardx.getBoard(start.x + 1, start.y + 1) == -1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y + 2) == stop)
                return true;
            if (boardx.getBoard(start.x - 1, start.y - 1) == -2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y - 2) == stop)
                return true;
            if (boardx.getBoard(start.x - 1, start.y + 1) == -2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y + 2) == stop)
                return true;
            if (boardx.getBoard(start.x + 1, start.y - 1) == -2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y - 2) == stop)
                return true;
            if (boardx.getBoard(start.x + 1, start.y + 1) == -2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y + 2) == stop)
                return true;
        }
        if(boardx.getBoard(start.x,start.y)==2)
        {
            for(int i=0;i<8;i++)
            {
                if (boardx.getBoard(start.x - i, start.y - i) == -1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y - i-1) == stop)
                    return true;
                if (boardx.getBoard(start.x - i, start.y + i) == -1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y + i+1) == stop)
                    return true;
                if (boardx.getBoard(start.x + i, start.y - i) == -1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y - i-1) == stop)
                    return true;
                if (boardx.getBoard(start.x + i, start.y + i) == -1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y + i+1) == stop)
                    return true;
                if (boardx.getBoard(start.x - i, start.y - i) == -2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y - i-1) == stop)
                    return true;
                if (boardx.getBoard(start.x - i, start.y + i) == -2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y + i+1) == stop)
                    return true;
                if (boardx.getBoard(start.x + i, start.y - i) == -2 && boardx.getBoard(stop.x, stop.y) == 0&&
                    Vector2i(start.x + i+1, start.y - i-1) == stop)
                    return true;
                if (boardx.getBoard(start.x + i, start.y + i) == -2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y + i+1) == stop)
                    return true;

            }
        }

    }
    if(!color) {
        if (boardx.getBoard(start.x, start.y) == -1) {
            if (boardx.getBoard(start.x - 1, start.y - 1) == 1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y - 2) == stop)
                return true;
            if (boardx.getBoard(start.x - 1, start.y + 1) == 1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y + 2) == stop)
                return true;
            if (boardx.getBoard(start.x + 1, start.y - 1) == 1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y - 2) == stop)
                return true;
            if (boardx.getBoard(start.x + 1, start.y + 1) == 1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y + 2) == stop)
                return true;
            if (boardx.getBoard(start.x - 1, start.y - 1) == 2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y - 2) == stop)
                return true;
            if (boardx.getBoard(start.x - 1, start.y + 1) == 2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x - 2, start.y + 2) == stop)
                return true;
            if (boardx.getBoard(start.x + 1, start.y - 1) == 2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y - 2) == stop)
                return true;
            if (boardx.getBoard(start.x + 1, start.y + 1) == 2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                Vector2i(start.x + 2, start.y + 2) == stop)
                return true;
        }
        if (boardx.getBoard(start.x, start.y) == -2) {
            for(int i=0;i<8;i++)
            {
                if (boardx.getBoard(start.x - i, start.y - i) == 1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y - i-1) == stop)
                    return true;
                if (boardx.getBoard(start.x - i, start.y + i) == 1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y + i+1) == stop)
                    return true;
                if (boardx.getBoard(start.x + i, start.y - i) == 1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y - i-1) == stop)
                    return true;
                if (boardx.getBoard(start.x + i, start.y + i) == 1 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y + i+1) == stop)
                    return true;
                if (boardx.getBoard(start.x - i, start.y - i) == 2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y - i-1) == stop)
                    return true;
                if (boardx.getBoard(start.x - i, start.y + i) == 2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x - i-1, start.y + i+1) == stop)
                    return true;
                if (boardx.getBoard(start.x + i, start.y - i) == 2 && boardx.getBoard(stop.x, stop.y) == 0&&
                    Vector2i(start.x + i+1, start.y - i-1) == stop)
                    return true;
                if (boardx.getBoard(start.x + i, start.y + i) == 2 && boardx.getBoard(stop.x, stop.y) == 0 &&
                    Vector2i(start.x + i+1, start.y + i+1) == stop)
                    return true;
            }
        }
    }
    return false;
}
