# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/hubert/pamsi-hubert-rudzik/Projekt3-Algorytmy/PAMSIGrafy/src/main.cpp" "/home/hubert/pamsi-hubert-rudzik/Projekt3-Algorytmy/PAMSIGrafy/cmake-build-debug/CMakeFiles/efficiency_tests.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DATA_DIR_PATH=\"/home/hubert/pamsi-hubert-rudzik/Projekt3-Algorytmy/PAMSIGrafy/mst_data/graph\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/hubert/pamsi-hubert-rudzik/Projekt3-Algorytmy/PAMSIGrafy/cmake-build-debug/CMakeFiles/graph_algorithms_lib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
