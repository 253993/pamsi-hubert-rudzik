
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/hubert/pamsi-hubert-rudzik/Projekt3-Algorytmy/PAMSIGrafy/src/minimum_spanning_tree_algorithms.cpp" "CMakeFiles/test_mst.dir/src/minimum_spanning_tree_algorithms.cpp.o" "gcc" "CMakeFiles/test_mst.dir/src/minimum_spanning_tree_algorithms.cpp.o.d"
  "/home/hubert/pamsi-hubert-rudzik/Projekt3-Algorytmy/PAMSIGrafy/src/mst_test_graphs.cpp" "CMakeFiles/test_mst.dir/src/mst_test_graphs.cpp.o" "gcc" "CMakeFiles/test_mst.dir/src/mst_test_graphs.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/hubert/pamsi-hubert-rudzik/Projekt3-Algorytmy/PAMSIGrafy/build/CMakeFiles/graph_algorithms_lib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
