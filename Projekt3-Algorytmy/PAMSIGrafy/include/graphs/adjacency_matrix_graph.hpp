#ifndef ADJACENCY_MATRIX_GRAPH_HPP_
#define ADJACENCY_MATRIX_GRAPH_HPP_

#include <vector>
#include "graphs/graph.hpp"

class AdjacencyMatrixGraph : public Graph
{
    std::vector<Edges> edges_list;
    std::vector<Vertices>vertices_list;
    std::vector<std::vector<Edges*>> matrix;//tablica 2D skłądająca się ze wskaźników na krawędzie
  public:
    virtual ~AdjacencyMatrixGraph();
    static std::unique_ptr<Graph> createGraph(std::istream& is);
    static bool SortByCost(const Edges& lhs, const Edges& rhs){
        return lhs.cost < rhs.cost;
    }

    virtual Vertices endVertices(Edges e)override; //zwraca końcowy wierzchołek krawędzi
    virtual Vertices opposite(Vertices v,Edges e)override; // zwraca przewciwległy do v względem e
    virtual bool areAdjacent(Vertices v,Vertices w) override;    //zwraca true jeśli są sąsiednie
    virtual void replace(Vertices v,Vertices x)override;    //zamienia wierzchołek v na wierzchołek x
    virtual void replace(Edges e,Edges x)override;  //zamienia krawędź e na krawędź x
    virtual void insertVertex(Vertices o)override;    //dodaje wierzchołek przecowującye element o
    virtual void insertEdge(Vertices v,Vertices w,int o)override; //dodaje krawędź między krawędziami v,w o długości o
    virtual void removeVertex(Vertices v)override;    //usuwa wierzchołek V oraz przylegające krawędzie.
    virtual void removeEdge(Edges e)override; //usuwa krawędź e.

    virtual void sort()override;
    virtual std::vector<Edges> getEdges(){return edges_list;}
    virtual std::vector<Vertices> getVertices(){return vertices_list;}
    virtual std::vector<std::vector<Edges*>> getMatrix(){return matrix;}
    virtual int getVerticesNum(){return vertices_num;}
    virtual int getEdgesNum(){return edges_num;}
    virtual bool circle(int v, bool* visited,bool *recStack);

};

#endif /* ADJACENCY_MATRIX_GRAPH_HPP_ */
