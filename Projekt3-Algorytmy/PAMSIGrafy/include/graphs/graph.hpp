#ifndef GRAPH_HPP_
#define GRAPH_HPP_

#include <fstream>
#include <iostream>
#include <string>
#include <memory>
#include <algorithm>

struct Vertices{
    int index;
    std::vector<struct Edges*> Incy;
};
struct Edges{
    Vertices *stop,*start;
    int cost;
    std::vector<Edges*> IncyStart;
    std::vector<Edges*> IncyStop;
};
static int vertices_num,edges_num;

class Graph
{
  public:

    virtual Vertices endVertices(Edges e)=0; //zwraca końcowy wierzchołek ostatniej krawędzi
    virtual Vertices opposite(Vertices,Edges)=0; // zwraca przewciwległy do v względem e
    virtual bool areAdjacent(Vertices v,Vertices w)=0;    //zwraca true jeśli są sąsiednie
    virtual void replace(Vertices v,Vertices x)=0;    //zamienia wierzchołek v na wierzchołek x
    virtual void replace(Edges e,Edges x)=0;  //zamienia krawędź e na krawędź x
    virtual void insertVertex(Vertices o)=0;    //dodaje wierzchołek przecowującye element o
    virtual void insertEdge(Vertices v,Vertices w,int o)=0; //dodaje krawędź między krawędziami v,w o długości o
    virtual void removeVertex(Vertices v)=0;    //usuwa wierzchołek V oraz przylegające krawędzie.
    virtual void removeEdge(Edges e)=0; //usuwa krawędź e.


    virtual void sort()=0;
    virtual std::vector<Edges> getEdges()=0;
    virtual std::vector<Vertices> getVertices()=0;
    virtual int getVerticesNum()=0;
    virtual int getEdgesNum()=0;
    virtual bool circle(int v, bool* visited,bool *recStack)=0;
};

#endif /* GRAPH_HPP_ */
