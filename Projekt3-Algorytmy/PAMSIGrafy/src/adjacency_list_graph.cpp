#include <filesystem>
#include "graphs/adjacency_list_graph.hpp"

AdjacencyListGraph::~AdjacencyListGraph(){
          edges_list.clear();
          vertices_list.clear();
          Incy_list->clear();
}
std::unique_ptr<Graph> AdjacencyListGraph::createGraph(std::istream& is)
{
    if(!is)
        std::cerr << "Can't open file.";

    AdjacencyListGraph graph;
    is >> vertices_num;
    is >> edges_num;

    graph.Incy_list=new std::vector<Edges*> [vertices_num];

    while(is)
    {
        auto * new_Edge=new Edges;
        auto * new_vertice1=new Vertices;
        auto * new_vertice2=new Vertices;

        is>>new_vertice1->index;
        is>>new_vertice2->index;
        is>>new_Edge->cost;

        new_Edge->start=new_vertice1;
        new_Edge->stop=new_vertice2;

        new_vertice1->Incy = graph.Incy_list[new_vertice1->index];
        new_vertice2->Incy = graph.Incy_list[new_vertice2->index];

        graph.Incy_list[new_vertice1->index].push_back(new_Edge);
        graph.Incy_list[new_vertice2->index].push_back(new_Edge);

        new_Edge->IncyStart=graph.Incy_list[new_Edge->start->index];
        new_Edge->IncyStop=graph.Incy_list[new_Edge->stop->index];


        for(int i=0;i<size(graph.vertices_list);i++)
        {
            if(graph.vertices_list[i].index==new_vertice1->index)
            {
                new_vertice1 = nullptr;
                break;
            }
        }
        for(int i=0;i<size(graph.vertices_list);i++)
        {
            if(graph.vertices_list[i].index == new_vertice2->index)
            {
                new_vertice2 = nullptr;
                break;
            }
        }
        if(new_vertice1!= nullptr){
            graph.vertices_list.push_back(*new_vertice1);
        }
        if(new_vertice2!= nullptr){
            graph.vertices_list.push_back(*new_vertice2);
        }
        graph.edges_list.push_back(*new_Edge);

    }
    return std::make_unique<AdjacencyListGraph>(graph);
}
Vertices AdjacencyListGraph::endVertices(Edges e)
{
    return *e.stop;
}
Vertices AdjacencyListGraph::opposite(Vertices v, Edges e)
{
    if(e.start == &v)
        return *e.stop;
    else
        return *e.start;
}
bool AdjacencyListGraph::areAdjacent(Vertices v, Vertices w)
{
    for(int i=0;i<edges_num;i++)
    {
            if((edges_list[i].stop->index==v.index && edges_list[i].start->index == w.index)
            ||(edges_list[i].stop->index==w.index && edges_list[i].start->index == v.index))
                return true;
    }
    return false;
}
void AdjacencyListGraph::replace(Edges e, Edges x)
{
    for(int i=0;i<vertices_num;i++)
    {
        for(int j=0;j<vertices_num;j++)
        {
            if(Incy_list[i][j] == &e)
            {
                Incy_list[i][j] = &x;
            }
        }
    }
}
void AdjacencyListGraph::replace(Vertices v, Vertices x)
{
    for(int i=0;i<vertices_num;i++)
    {
        for(int j=0;j<vertices_num;j++)
        {
            if(Incy_list[i][j]->start == &v)
                Incy_list[i][j]->start = &x;
            if(Incy_list[i][j]->stop == &v)
                Incy_list[i][j]->stop = &x;
        }
    }
}
void AdjacencyListGraph::insertVertex(Vertices o){
    vertices_list.push_back(o);
}
void AdjacencyListGraph::insertEdge(Vertices v, Vertices w, int o)
{
    Edges Edge;
    Edge.start=&v;
    Edge.stop=&w;
    Edge.cost=o;
    Incy_list[v.index][w.index]=&Edge;
}
void AdjacencyListGraph::removeEdge(Edges e)
{
    for(int i=0;i<vertices_num;i++)
    {
        if(&edges_list[i] == &e)
        {
            Edges temp=edges_list[size(edges_list)];
            edges_list[size(edges_list)]=edges_list[i];
            edges_list.pop_back();
            edges_list[i]=temp;
        }
        for(int j=0;j<vertices_num;j++)
        {
            if(Incy_list[i][j]==&e)
            {
                Incy_list[i][j]= nullptr;

            }
        }
    }
}
void AdjacencyListGraph::removeVertex(Vertices v)
{
    for(int i=0;i<vertices_num;i++)
    {
        if(v.index==vertices_list[i].index)
        {
            Vertices temp=vertices_list[size(vertices_list)];
            vertices_list[size(vertices_list)]=vertices_list[i];
            vertices_list.pop_back();
            vertices_list[i]=temp;
        }
    }
    for(int i=0;i<edges_num;i++)
    {
        if(edges_list[i].start->index==v.index)
            edges_list[i].start= nullptr;
        if(edges_list[i].stop->index==v.index)
            edges_list[i].stop= nullptr;
    }
}
void AdjacencyListGraph::sort(){
    std::sort(edges_list.begin(),edges_list.end(),SortByCost);
}
bool AdjacencyListGraph::circle(int v, bool* visited,bool *recStack)
{
    if(!visited[v])
    {
        visited[v] = true;
        recStack[v] = true;
        for(int i=0; i<size(vertices_list); i++)
        {
            if ( (!visited[i] && circle(i, visited, recStack) )|| recStack[i])
                return true;
        }
    }
    recStack[v] = false;
    return false;
}