#include "graphs/adjacency_matrix_graph.hpp"

AdjacencyMatrixGraph::~AdjacencyMatrixGraph(){
   edges_list.clear();
   vertices_list.clear();
   matrix.clear();
}

std::unique_ptr<Graph> AdjacencyMatrixGraph::createGraph(std::istream& is)
{
    if(!is) std::cerr<<"Can't open file.";
    AdjacencyMatrixGraph graph;
    is>>vertices_num;
    is>>edges_num;

    graph.matrix.resize(vertices_num);
    for(int i=0;i<vertices_num;i++){
        graph.matrix[i].resize(vertices_num);
    }

    while(is)
    {
      auto * new_edge=new Edges;
      auto * new_vertice1=new Vertices;
      auto * new_vertice2=new Vertices;

      is>>new_vertice1->index;
      is>>new_vertice2->index;
      is>>new_edge->cost;

      new_edge->start=new_vertice1;
      new_edge->stop=new_vertice2;
        for(int i=0;i<size(graph.vertices_list);i++)
        {
            if(graph.vertices_list[i].index==new_vertice1->index)
            {
                new_vertice1 = nullptr;
                break;
            }
        }
        for(int i=0;i<size(graph.vertices_list);i++)
        {
            if(graph.vertices_list[i].index == new_vertice2->index)
            {
                new_vertice2 = nullptr;
                break;
            }
        }
        if(new_vertice1!= nullptr){
            graph.vertices_list.push_back(*new_vertice1);
        }
        if(new_vertice2!= nullptr){
            graph.vertices_list.push_back(*new_vertice2);
        }
        graph.edges_list.push_back(*new_edge);
        for(int i=0;i<vertices_num;i++){
            for(int j=0;j<vertices_num;j++)
            {
                if((new_edge->start->index==i&&new_edge->stop->index==j) ||(new_edge->stop->index==i &&new_edge->start->index==j))
                graph.matrix[i][j] = new_edge;
            }
    }
}
    return std::make_unique<AdjacencyMatrixGraph>(graph);
}
Vertices AdjacencyMatrixGraph::endVertices(Edges e)
{
    return *e.stop;
}
Vertices AdjacencyMatrixGraph::opposite(Vertices v, Edges e)
{
    if(e.start == &v)
        return *e.stop;
    else
        return *e.start;
}
bool AdjacencyMatrixGraph::areAdjacent(Vertices v, Vertices w)
{
    for(int i=0;i<vertices_num;i++)
    {
        for(int j=0;j<vertices_num;j++)
        if((matrix[i][j]->stop==&v && matrix[i][j]->start == &w)
           ||(matrix[i][j]->stop==&w && matrix[i][j]->start == &v))
            return true;
    }
    return false;
}
void AdjacencyMatrixGraph::replace(Edges e, Edges x)
{
    for(int i=0;i<vertices_num;i++)
    {
        for(int j=0;j<vertices_num;j++)
        {
            if(matrix[i][j]==&e)
            {
                matrix[i][j] = &x;
            }
        }
    }
}
void AdjacencyMatrixGraph::replace(Vertices v, Vertices x)
{
    for(int i=0;i<vertices_num;i++)
    {
        for(int j=0;j<vertices_num;j++)
        {
            if(matrix[i][j]->start==&v)
                matrix[i][j]->start=&x;
            if(matrix[i][j]->stop==&v)
                matrix[i][j]->stop=&x;
        }
    }
}
void AdjacencyMatrixGraph::insertVertex(Vertices o)
{
    vertices_list.push_back(o);
}
void AdjacencyMatrixGraph::insertEdge(Vertices v, Vertices w, int o)
{
    Edges Edge;
    Edge.start=&v;
    Edge.stop=&w;
    Edge.cost=o;
    matrix[v.index][w.index]=&Edge;
}
void AdjacencyMatrixGraph::removeEdge(Edges e)
{
    for(int i=0;i<vertices_num;i++)
    {
        if(&edges_list[i] == &e)
        {
            Edges temp=edges_list[size(edges_list)];
            edges_list[size(edges_list)]=edges_list[i];
            edges_list.pop_back();
            edges_list[i]=temp;
        }
        for(int j=0;j<vertices_num;j++)
        {
            if(matrix[i][j]==&e)
            {
                matrix[i][j]= nullptr;

            }
        }
    }
}

void AdjacencyMatrixGraph::removeVertex(Vertices v)
{
    for(int i=0;i<vertices_num;i++)
    {
        if(v.index==vertices_list[i].index)
        {
            Vertices temp=vertices_list[size(vertices_list)];
            vertices_list[size(vertices_list)]=vertices_list[i];
            vertices_list.pop_back();
            vertices_list[i]=temp;
        }
    }
    for(int i=0;i<edges_num;i++)
    {
        if(edges_list[i].start->index==v.index)
            edges_list[i].start= nullptr;
        if(edges_list[i].stop->index==v.index)
            edges_list[i].stop= nullptr;
    }
}

void AdjacencyMatrixGraph::sort(){
    std::sort(edges_list.begin(),edges_list.end(), SortByCost);
}
bool AdjacencyMatrixGraph::circle(int v, bool* visited, bool *recStack)
{
        if(!visited[v])
        {
            visited[v] = true;
            recStack[v] = true;
            for(int i=0; i<size(matrix[v]); i++)
            {
                if (( !visited[i] && circle(i, visited, recStack))||recStack[i])
                    return true;
            }
        }
        recStack[v] = false;  // remove the vertex from recursion stack
        return false;
}